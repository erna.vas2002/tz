const treeView = document.getElementById("treeView");

getFetchServices();

async function getFetchServices() {
    const services = await axios.get("http://localhost:3000/services");

    treeView.appendChild(buildTree(services.data, null))
}

function buildTree(data, parentId = null) {
  const ul = document.createElement("ul");

    data
    .filter((entry) => entry.head === parentId)
    .sort((a, b) => a.sorthead - b.sorthead)
    .forEach((child) => {
      const li = document.createElement("li");

      li.textContent = child.name;
      ul.appendChild(li);

      if (child.node === 1) {
        li.appendChild(buildTree(data, child.id));
      }

    });
    
  return ul;
}
